import React, { useContext } from 'react';
import { createContext } from 'react';
import { messages } from './string.const';
const ctx = createContext(messages);

export function StringProvider({ children }) {
  return <ctx.Provider value={messages}>{children}</ctx.Provider>;
}

export function useStringProvider() {
  return useContext(ctx);
}
