import React, { createContext, useContext } from 'react';
import useAuth from './auth.hook';

const ctx = createContext({
  isAuth: false,
  loading: false,
  error: undefined,
  auth: (name, password) => {},
});

export function AuthProvider({ children }) {
  const auth = useAuth();
  return <ctx.Provider value={auth}>{children}</ctx.Provider>;
}

export function useAuthProvider() {
  return useContext(ctx);
}
