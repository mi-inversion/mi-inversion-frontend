import { useState } from 'react';
import { useStringProvider } from './string.provider';

function useAuth() {
  const {
    login: { AUTH_ERROR },
  } = useStringProvider();
  const [isAuth, setIsAuth] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(undefined);

  function auth(name, password) {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setIsAuth(true);
      setError(AUTH_ERROR);
    }, 1000);
  }

  return {
    isAuth,
    loading,
    error,
    auth,
  };
}

export default useAuth;
