export const messages = {
  login: {
    LOGIN_WELCOME: 'Bienvenido',
    NAME_LOGIN_PLACEHOLDER: 'Nombre de usuario',
    PASSWORD_LOGIN_PLACEHOLDER: 'Contraseña',
    ACCESS_LOGIN_BUTTON: 'Acceder',
    AUTH_ERROR: 'Error al autenticar contraseña o nombre incorrecto',
  },
  sections: {
    rendimiento: {
      TITLE: 'Rendimiento',
      PLAN_PLACEHOLDER: 'Plan',
      MONTO_PLACEHOLDER: 'Monto ($)',
      OPEN_DATE_PLACEHOLDER: 'Fecha de apertura',
      GENERATE_BUTTON: 'Generar',
      RENDIMIENTO_TOTAL: 'Rendimiento total',
      PAGO_TOTAL: 'Pago total',
      PRIMER_PAGO: 'Primer pago',
      PAGOS: 'Pagos',
    },
  },
  app: {
    APP_CRUD_BUTTON: 'Crear nuevo',
  },
  layout: {
    HELLO_MESSAGE: (name) => `¡Hola ${name}!`,
  },
};
