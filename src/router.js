import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Login from './routes/login';
import Playground from './routes/playground';
import Dashboard from './routes/dashboard';

function MainRouter() {
  return (
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/playground" component={Playground} />
      <Route path="/" component={Dashboard} />
      <Route path="*">
        <Redirect to="/" />
      </Route>
    </Switch>
  );
}

export default MainRouter;
