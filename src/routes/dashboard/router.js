import React from 'react';
import Planes from './routes/planes';
import Rendimiento from './routes/rendimiento';
import { Redirect, Switch, Route } from 'react-router-dom';

function DashboardRouter() {
  return (
    <Switch>
      <Route path="/rendimiento" component={Rendimiento} />
      <Route path="/planes" component={Planes} />
      <Route path="*">
        <Redirect to="/rendimiento" />
      </Route>
    </Switch>
  );
}

export default DashboardRouter;
