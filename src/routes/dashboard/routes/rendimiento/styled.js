import styled from 'styled-components';
import { Grid } from '@material-ui/core';

export const FullGrid = styled(Grid)`
  width: 100%;
`;
