import React from 'react';
import { Grid, TextField } from '@material-ui/core';
import InlineForm from '../../../../components/inline-form';
import useBasicInlineForm from '../../../../components/inline-form/hooks/useBasicInlineForm';
import { useStringProvider } from '../../../../providers/string.provider';
import RendimientoInfo from '../../../../components/rendimiento-info';
import { FullGrid } from './styled';
import { AnimationBottomToUp } from '../../../../components/animations';

function Rendimiento() {
  const {
    sections: {
      rendimiento: {
        GENERATE_BUTTON,
        PLAN_PLACEHOLDER,
        MONTO_PLACEHOLDER,
        OPEN_DATE_PLACEHOLDER,
      },
    },
  } = useStringProvider();

  const { form } = useBasicInlineForm(
    GENERATE_BUTTON,
    [
      {
        FormType: TextField,
        label: PLAN_PLACEHOLDER,
        name: 'plan',
      },
      {
        FormType: TextField,
        label: MONTO_PLACEHOLDER,
        name: 'monto',
      },
      {
        FormType: TextField,
        label: OPEN_DATE_PLACEHOLDER,
        name: 'date-to-open',
      },
    ],
    (data) => {},
  );

  return (
    <Grid container spacing={6} direction="column" alignItems="center">
      <FullGrid item>
        <AnimationBottomToUp
          time={0.5}
          delay={0.1}
          easing="cubic-bezier(0.075, 0.82, 0.165, 1)">
          <InlineForm {...form} />
        </AnimationBottomToUp>
      </FullGrid>
      <FullGrid item>
        <AnimationBottomToUp
          time={0.5}
          delay={0.2}
          easing="cubic-bezier(0.075, 0.82, 0.165, 1)">
          <RendimientoInfo
            title="Plan active plus"
            rendimientoTotal="$ 7,200"
            pagoTotal="$ 19,200"
            primerPago="$1600 - 19 / Enero / 2020"
            pagos={[
              '$1600 - 19 / Enero / 2020',
              '$1600 - 19 / Enero / 2020',
              '$1600 - 19 / Enero / 2020',
            ]}
          />
        </AnimationBottomToUp>
      </FullGrid>
    </Grid>
  );
}

export default Rendimiento;
