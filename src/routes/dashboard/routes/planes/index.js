import React from 'react';
import AppCrud from '../../../../components/app-crud';
import useBasicAppCrud from '../../../../components/app-crud/hooks/useBasicAppCrud';

function Planes() {
  const { appCrud } = useBasicAppCrud(
    [
      {
        name: 'Plan plus',
        minInvest: '10%',
        maxInvest: '20%',
        monthlyRate: '20',
        planDuration: '20',
        performanceDelivery: '20',
      },
    ],
    [
      {
        name: 'Nombre del plan',
        render: (item) => item.name,
        propName: 'name',
        type: 'text',
      },
      {
        name: 'Inversión Mínima ($)',
        render: (item) => item.minInvest,
        propName: 'minInvest',
        type: 'number',
      },
      {
        name: 'Inversión Maxima ($)',
        propName: 'maxInvest',
        render: (item) => item.maxInvest,
        type: 'number',
      },
      {
        name: 'Tasa Mensual (%)',
        propName: 'monthlyRate',
        render: (item) => item.monthlyRate,
        type: 'number',
      },
      {
        name: 'Duración del plan (Meses)',
        propName: 'planDuration',
        render: (item) => item.planDuration,
        type: 'number',
      },
      {
        name: 'Entrega de rendimiento (Meses)',
        propName: 'performanceDelivery',
        render: (item) => item.performanceDelivery,
        type: 'number',
      },
    ],
    () => {},
    () => {},
    () => {},
    false,
  );

  return <AppCrud {...appCrud} />;
}

export default Planes;
