import React from 'react';
import RendimientoIcon from '@material-ui/icons/Timeline';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
export const layoutItems = [
  {
    name: 'Rendimiento',
    path: '/rendimiento',
    icon: <RendimientoIcon />,
  },
  {
    name: 'Planes',
    path: '/planes',
    icon: <ImportContactsIcon />,
  },
];
