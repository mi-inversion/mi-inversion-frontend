import React from 'react';
import { useAuthProvider } from '../../providers/auth.provider';
import { Redirect } from 'react-router-dom';
import Layout from '../../components/layout';
import { useStringProvider } from '../../providers/string.provider';
import useBasicLayout from '../../components/layout/hooks/useBasicLayout';
import { layoutItems } from './layout-items';
import DashboardRouter from './router';

function Dashboard() {
  const { isAuth } = useAuthProvider();
  const {
    layout: { HELLO_MESSAGE },
  } = useStringProvider();

  const { layout } = useBasicLayout(layoutItems);

  if (!isAuth) return <Redirect to="/login" />;

  return (
    <Layout helloMessage={HELLO_MESSAGE('Alberto')} {...layout}>
      <DashboardRouter />
    </Layout>
  );
}

export default Dashboard;
