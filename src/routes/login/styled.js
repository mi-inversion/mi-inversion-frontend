import styled, { keyframes } from 'styled-components';
import AppCard from '../../components/app-card';

const showLoading = keyframes`
    from{
        transform:scale(1.2);
        opacity:0;
    }
    to{
        transform:scale(1);
        opacity:1;
    }
`;

export const LoginContainer = styled.div`
  min-width: 100vw;
  min-height: 100vh;
  width: fit-content;
  height: fit-content;
  padding: 10px;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LoginTopBar = styled.div`
  position: absolute;
  min-width: 100vw;
  min-height: 50vh;
  background: #3ec9b7;
  top: 0;
  z-index: -1;
`;

export const LoadingContainer = styled(AppCard)`
  animation: ${showLoading} 0.3s;
`;
