import React from 'react';
import { LoginContainer, LoginTopBar, LoadingContainer } from './styled';
import LoginForm from '../../components/login-form';
import useBasicLoginForm from '../../components/login-form/hooks/useBasicLoginForm';
import { useAuthProvider } from '../../providers/auth.provider';
import { CircularProgress } from '@material-ui/core';
import { Redirect } from 'react-router-dom';

function Login() {
  const { auth, loading, error, isAuth } = useAuthProvider();
  const { loginForm } = useBasicLoginForm(auth);

  if (isAuth) return <Redirect to="/" />;

  return (
    <LoginContainer>
      <LoginTopBar />
      {loading ? (
        <LoadingContainer>
          <CircularProgress />
        </LoadingContainer>
      ) : (
        <LoginForm error={error} {...loginForm} />
      )}
    </LoginContainer>
  );
}

export default Login;
