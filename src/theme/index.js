import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      contained: {
        color: 'white !important',
        textTransform: 'none',
      },
    },
  },
  palette: {
    primary: {
      main: '#3EC9B7',
    },
    text: {
      primary: '#707070',
    },
  },
  typography: {
    allVariants: {
      fontFamily: "'Nunito', sans-serif",
    },
    h2: {
      color: '#C2C2C2',
    },
    button: {
      textTransform: 'none',
      paddingTop: '8px',
      fontSize: '1.1em',
    },
  },
});

export default theme;
