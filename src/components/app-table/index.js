import React, { useState } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  Menu,
  IconButton,
} from '@material-ui/core';
import Action from './components/action';
import MoreVertIcon from '@material-ui/icons/MoreVert';

function AppTable({ columns, data, actions, onAction }) {
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [selectedItem, setSelectedItem] = React.useState(null);
  const [options, setOptions] = React.useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleAction(name, item) {
    handleClose();
    onAction(name, item);
  }

  function handleClose() {
    setAnchorEl(null);
  }
  const [page, setPage] = useState(0);
  const handlePageChange = (event, page) => {
    setPage(page);
  };
  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(event.target.value);
  };
  return (
    <div>
      <Table>
        <TableHead>
          <TableRow>
            {columns.map(({ name }, index) => (
              <TableCell key={index}>{name}</TableCell>
            ))}
            {actions && <TableCell></TableCell>}
          </TableRow>
        </TableHead>
        <TableBody>
          {data
            .slice(rowsPerPage * page, rowsPerPage * (page + 1))
            .map((item, index) => (
              <TableRow key={index}>
                {columns.map(({ render }, index) => (
                  <TableCell key={index}>{render(item)}</TableCell>
                ))}
                {actions && (
                  <TableCell align="left">
                    <IconButton
                      aria-label="More"
                      aria-controls="long-menu"
                      aria-haspopup="true"
                      onClick={(ev) => {
                        handleClick(ev);
                        setSelectedItem(item);
                        setOptions(
                          actions.map((action, index) => (
                            <Action
                              key={index}
                              {...{
                                ...action,
                                selectedItem,
                                onAction: handleAction,
                                item,
                              }}
                            />
                          )),
                        );
                      }}>
                      <MoreVertIcon />
                    </IconButton>
                  </TableCell>
                )}
              </TableRow>
            ))}
        </TableBody>
      </Table>
      <TablePagination
        component="div"
        count={data.length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleRowsPerPageChange}
        page={page}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={[5, 10, 25]}
      />
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}>
        {options}
      </Menu>
    </div>
  );
}

export default AppTable;
