import React from 'react';
import { MenuItem } from '@material-ui/core';

function Action({
  name,
  label,
  color,
  variant,
  customComponent: CustomComponent,
  item,
  onAction,
  style,
}) {
  return CustomComponent ? (
    <CustomComponent
      item={item}
      onClick={() => {
        onAction(name, item);
      }}
    />
  ) : (
    <MenuItem
      variant={variant}
      onClick={() => {
        onAction(name, item);
      }}
      style={style}
      color={color}>
      {label}
    </MenuItem>
  );
}

export default Action;
