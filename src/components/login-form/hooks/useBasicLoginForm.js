import { useState } from 'react';

function useBasicLoginForm(onAccess) {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  function changeName({ target: { value } }) {
    setName(value);
  }

  function changePassword({ target: { value } }) {
    setPassword(value);
  }

  function access() {
    onAccess(name, password);
  }

  return {
    loginForm: {
      onNamechange: changeName,
      onPasswordChange: changePassword,
      name,
      password,
      onAccess: access,
    },
  };
}

export default useBasicLoginForm;
