import React from 'react';
import { LoginCard, TextFieldContainer, ButtonContainer } from './styled';
import { Grid, Typography, TextField } from '@material-ui/core';
import { useStringProvider } from '../../providers/string.provider';
import ProfileImage from '../profile-image';
import AppButton from '../app-button';

function LoginForm({
  onNamechange,
  onPasswordChange,
  name,
  password,
  onAccess,
  error,
}) {
  const {
    login: {
      LOGIN_WELCOME,
      NAME_LOGIN_PLACEHOLDER,
      PASSWORD_LOGIN_PLACEHOLDER,
      ACCESS_LOGIN_BUTTON,
    },
  } = useStringProvider();
  return (
    <LoginCard>
      <Grid
        container
        spacing={3}
        direction="column"
        justify="center"
        alignItems="center">
        <Grid item>
          <ProfileImage />
        </Grid>
        <Grid item>
          <Typography color="textPrimary" variant="h3">
            {LOGIN_WELCOME}
          </Typography>
        </Grid>
        <TextFieldContainer item>
          <TextField
            onInput={onNamechange}
            label={NAME_LOGIN_PLACEHOLDER}
            fullWidth
            value={name}
          />
        </TextFieldContainer>
        <TextFieldContainer item>
          <TextField
            onInput={onPasswordChange}
            type="password"
            label={PASSWORD_LOGIN_PLACEHOLDER}
            value={password}
            fullWidth
          />
        </TextFieldContainer>
        {error ? (
          <Grid item>
            <Typography variant="caption" color="error">
              {error}
            </Typography>
          </Grid>
        ) : null}
        <ButtonContainer item>
          <AppButton onClick={onAccess}>{ACCESS_LOGIN_BUTTON}</AppButton>
        </ButtonContainer>
      </Grid>
    </LoginCard>
  );
}

export default LoginForm;
