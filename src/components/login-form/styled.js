import styled, { keyframes } from 'styled-components';
import AppCard from '../app-card';
import { Grid } from '@material-ui/core';

const showForm = keyframes`
    from{
        opacity:0;
        transform:scaleY(0);
    }
    to{
        opacity:1;
        transform:scaleY(1);
    }
`;
const showFormContent = keyframes`
    from{
        opacity:0;
    }
    to{
        opacity:1;
    }
`;

export const LoginCard = styled(AppCard)`
  max-width: 380px;
  width: 380px;
  padding-bottom: 40px;
  opacity: 0;
  animation: ${showForm} 0.5s cubic-bezier(0.19, 1, 0.22, 1);
  animation-fill-mode: forwards;
  & > * {
    opacity: 0;
    animation: ${showFormContent} 0.5s;
    animation-delay: 0.3s;
    animation-fill-mode: forwards;
  }
`;

export const TextFieldContainer = styled(Grid)`
  width: 80%;
`;

export const ButtonContainer = styled(Grid)`
  margin-top: 20px;
  width: 80%;
`;
