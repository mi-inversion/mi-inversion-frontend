import React from 'react';
import { LayoutContainer, Title } from './styled';
import Sidebar from '../sidebar';

function Layout({ items, children, helloMessage, itemSelected, onItemSelect }) {
  return (
    <LayoutContainer>
      <Sidebar
        helloMessage={helloMessage}
        itemSelected={itemSelected}
        items={items}
        onItemSelect={onItemSelect}
      />
      <Title key={itemSelected} variant="h2">
        {items[itemSelected] && items[itemSelected].name}
      </Title>
      {children}
    </LayoutContainer>
  );
}

export default Layout;
