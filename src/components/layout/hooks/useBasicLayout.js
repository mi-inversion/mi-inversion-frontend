import { useHistory } from 'react-router-dom';
function useBasicLayout(items) {
  const { location, push } = useHistory();

  const itemSelected = items.findIndex((item) =>
    location.pathname.startsWith(item.path),
  );

  function selectItem(index) {
    push(items[index].path);
  }

  return {
    layout: {
      items,
      itemSelected,
      onItemSelect: selectItem,
    },
  };
}

export default useBasicLayout;
