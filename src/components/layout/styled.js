import styled, { keyframes } from 'styled-components';
import { Typography } from '@material-ui/core';

const titleAnimation = keyframes`
    from{
        transform:translateX(-30px);
        opacity:0;
    }
    to{
        transform:translateX(0);
        opacity:1
    }
`;

export const LayoutContainer = styled.div`
  padding-left: 380px;
  padding-top: 50px;
  padding-right: 30px;
`;

export const Title = styled(Typography)`
  animation: ${titleAnimation} 0.3s;
  margin-bottom: 40px;
`;
