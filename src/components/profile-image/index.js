import React from 'react';
import { ProfileImageContainer, ProfileImageContent } from './styled';

function ProfileImage() {
  return (
    <ProfileImageContainer>
      <ProfileImageContent></ProfileImageContent>
    </ProfileImageContainer>
  );
}

export default ProfileImage;
