import styled from 'styled-components';

export const ProfileImageContainer = styled.div`
  width: 117px;
  height: 117px;
  background: white;
  box-shadow: 0px 3px 4px #00000017;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
`;

export const ProfileImageContent = styled.div`
  width: 109px;
  height: 109px;
  background: #f3f1ff;
  border-radius: 50%;
`;
