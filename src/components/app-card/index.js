import React from 'react';
import { CardContainer } from './styled';

function AppCard({ children, style, className }) {
  return <CardContainer {...{ className, style }}>{children}</CardContainer>;
}

export default AppCard;
