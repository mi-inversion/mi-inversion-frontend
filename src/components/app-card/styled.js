import styled from 'styled-components';

export const CardContainer = styled.div`
  background: white;
  padding: 20px;
  box-shadow: 0px 8px 25px #0000000f;
  border-radius: 8px;
`;
