import React from 'react';
import { InlineFormContainer } from './styled';
import { Button } from '@material-ui/core';

function InlineForm({
  style,
  className,
  formControls,
  onFormChange,
  formData,
  onSend,
  sendButtonLabel,
}) {
  return (
    <InlineFormContainer {...{ style, className }}>
      {formControls.map((formControl) => (
        <formControl.FormType
          label={formControl.label}
          value={formData[formControl.name]}
          variant="outlined"
          onInput={(ev) => {
            onFormChange(formControl.name, ev.target.value);
          }}
          fullWidth
          {...formControl}
        />
      ))}
      <Button
        size="large"
        style={{ minWidth: '180px' }}
        variant="outlined"
        onClick={onSend}>
        {sendButtonLabel}
      </Button>
    </InlineFormContainer>
  );
}

export default InlineForm;
