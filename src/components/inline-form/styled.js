import styled from 'styled-components';
import AppCard from '../app-card';

export const InlineFormContainer = styled(AppCard)`
  display: flex;
  justify-content: center;
  align-items: center;
  & > * {
    margin: 0 15px;
  }
`;
