import styled from 'styled-components';

export const ButtonContainer = styled.div`
  padding: 10px 10px;
  cursor: pointer;
  user-select: none;
  color: white;
  background: #3ec9b7;
  border-radius: 20px;
  text-align: center;
  transition: 0.2s;
  &:hover {
    box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.1);
  }
  &:active {
    transform: scale(0.96);
  }
`;
