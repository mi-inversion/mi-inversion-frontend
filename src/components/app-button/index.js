import React from 'react';
import { ButtonContainer } from './styled';
import { Typography } from '@material-ui/core';

function AppButton({ children, className, style, onClick }) {
  return (
    <ButtonContainer role="button" onClick={onClick} {...{ style, className }}>
      <Typography variant="body1">{children}</Typography>
    </ButtonContainer>
  );
}

export default AppButton;
