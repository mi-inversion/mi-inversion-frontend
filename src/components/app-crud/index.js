import React from 'react';
import {
  Grid,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import { useStringProvider } from '../../providers/string.provider';
import AppTable from '../app-table';
import AppCard from '../app-card';
import Form from '../form';
import { AnimationBottomToUp } from '../animations';
function AppCrud({
  data,
  properties,
  onCreate,
  onUpdate,
  onDelete,
  isFormOpen,
  onFormOpen,
  onFormClose,
  formType,
  onSetFormType,
  formData,
  onFormDataChange,
  formLoading,
  error,
  clearForm,
}) {
  const {
    app: { APP_CRUD_BUTTON },
  } = useStringProvider();

  function action(name, item) {
    if (name === 'delete') {
    }
    if (name === 'update') {
      onSetFormType('update');
      clearForm({});
      onFormOpen();
    }
  }

  const formTitle = formType === 'create' ? 'Crear' : 'Actualizar';
  console.log(formData);
  return (
    <Grid container spacing={6} direction="column" alignItems="center">
      <Grid item container justify="flex-end">
        <AnimationBottomToUp
          time={0.5}
          delay={0.1}
          easing="cubic-bezier(0.075, 0.82, 0.165, 1)">
          <Button
            disableElevation
            color="primary"
            style={{ width: '200px' }}
            variant="contained"
            onClick={() => {
              onSetFormType('create');
              clearForm();
              onFormOpen();
            }}>
            {APP_CRUD_BUTTON}
          </Button>
        </AnimationBottomToUp>
      </Grid>
      <Grid item style={{ width: '100%' }}>
        <AnimationBottomToUp
          time={0.5}
          delay={0.2}
          easing="cubic-bezier(0.075, 0.82, 0.165, 1)">
          <AppCard>
            <AppTable
              data={data}
              columns={properties}
              actions={[
                {
                  name: 'update',
                  label: 'Actualizar',
                  style: { color: 'gray' },
                },
                {
                  name: 'delete',
                  label: 'Eliminar',
                  style: { color: '#ef5350' },
                },
              ]}
              onAction={action}
            />
          </AppCard>
        </AnimationBottomToUp>
      </Grid>
      <Dialog open={isFormOpen} onClose={formLoading ? undefined : onFormClose}>
        <DialogTitle>
          <Typography color="textPrimary" variant="h4">
            {formTitle}
          </Typography>
        </DialogTitle>
        <DialogContent style={{ width: '400px' }}>
          <Form
            error={error}
            formData={formData}
            onFormChange={onFormDataChange}
            formControls={properties.map((item) => ({
              FormType: TextField,
              label: item.name,
              name: item.propName,
              type: item.type,
            }))}
          />
        </DialogContent>
        <DialogActions>
          <Button
            disabled={formLoading ? true : false}
            color="primary"
            onClick={onFormClose}
            variant="outlined">
            Cancelar
          </Button>
          <Button
            disableElevation
            style={{ width: '200px' }}
            color="primary"
            variant="contained"
            disabled={formLoading ? true : false}>
            {formLoading ? <CircularProgress size="27px" /> : formTitle}
          </Button>
        </DialogActions>
      </Dialog>
    </Grid>
  );
}

export default AppCrud;
