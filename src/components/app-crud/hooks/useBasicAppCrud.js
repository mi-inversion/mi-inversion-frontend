import { useState } from 'react';

function useBasicAppCrud(
  data,
  properties,
  onCreate,
  onUpdate,
  onDelete,
  formLoading,
  error,
) {
  const [formOpen, setFormOpen] = useState(false);
  const [formType, setFormType] = useState('');
  const [formData, setFormData] = useState({});

  function changeForm(name, value) {
    const newData = { ...formData };
    newData[name] = value;
    console.log(newData);
    setFormData(newData);
  }

  function clearForm() {
    setFormData({});
  }

  return {
    appCrud: {
      data,
      properties,
      onCreate,
      onUpdate,
      onDelete,
      isFormOpen: formOpen,
      onFormOpen: () => setFormOpen(true),
      onFormClose: () => setFormOpen(false),
      formType,
      onSetFormType: setFormType,
      formData,
      onFormDataChange: changeForm,
      formLoading,
      error,
      clearForm,
    },
  };
}

export default useBasicAppCrud;
