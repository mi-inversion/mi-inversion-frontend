import React from 'react';
import AppCard from '../app-card';
import { Grid, Typography } from '@material-ui/core';

function RendimientoInfo({
  title,
  rendimientoTotal,
  pagoTotal,
  primerPago,
  pagos,
}) {
  return (
    <AppCard style={{ paddingBottom: '50px' }}>
      <Grid spacing={3} container direction="column">
        <Grid item>
          <Typography color="primary" variant="h4">
            {title}
          </Typography>
        </Grid>
        <Grid item container>
          <Grid xs={6} spacint={2} item container direction="column">
            <Grid item>
              <Typography color="primary" variant="h6">
                Rendimiento total
              </Typography>
              <Typography color="primary" variant="h6">
                {rendimientoTotal}
              </Typography>
            </Grid>
            <br />
            <Grid item>
              <Typography color="primary" variant="h6">
                Pago total
              </Typography>
              <Typography color="primary" variant="h6">
                {pagoTotal}
              </Typography>
            </Grid>
            <br />
            <Grid item>
              <Typography color="primary" variant="h6">
                Primer pago
              </Typography>
              <Typography color="primary" variant="h6">
                {primerPago}
              </Typography>
            </Grid>
          </Grid>
          <Grid xs={6} item>
            <Typography color="primary" variant="h6">
              Pagos
            </Typography>
            <Typography color="primary" variant="h6">
              {pagos.map((pago) => (
                <div style={{ margin: '10px 0' }}>
                  {pago}
                  <br />
                </div>
              ))}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </AppCard>
  );
}

export default RendimientoInfo;
