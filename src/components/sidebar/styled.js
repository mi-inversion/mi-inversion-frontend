import styled from 'styled-components';

export const SidebarContainer = styled.div`
  background: white;
  min-height: 100vh;
  height: fit-content;
  width: 350px;
  padding: 20px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  flex-direction: column;
  border-radius: 0px 8px 8px 0px;
  position: fixed;
  left: 0;
  top: 0;
`;

export const ItemsContainer = styled.div`
  margin-top: 20px;
  width: 350px;
`;
