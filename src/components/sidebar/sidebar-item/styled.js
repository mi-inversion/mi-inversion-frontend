import styled from 'styled-components';

export const SidebarItemContainer = styled.div`
  padding: 10px;
  box-sizing: border-box;
  width: 100%;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  align-items: center;
  position: relative;
  transition: 0.1s;
  &:hover {
    background: rgba(0, 0, 0, 0.02);
  }
  & > * {
    margin-left: 10px;
  }
  &::after {
    content: '';
    left: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    position: absolute;
    height: 100%;
    width: 5px;
    background: #3ec9b7;
    transition: 0.2s;
    ${({ selected }) => (!selected ? 'transform: translateX(-100%);' : '')}
  }
`;
export const IconContainer = styled.div`
  color: #3ec9b7 !important;
`;
