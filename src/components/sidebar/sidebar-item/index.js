import React from 'react';
import { SidebarItemContainer, IconContainer } from './styled';
import { Typography } from '@material-ui/core';

function SidebarItem({ name, icon, onClick, isSelected }) {
  return (
    <SidebarItemContainer onClick={onClick} selected={isSelected}>
      <IconContainer>{icon}</IconContainer>

      <Typography variant="h6" color="textSecondary">
        {name}
      </Typography>
    </SidebarItemContainer>
  );
}

export default SidebarItem;
