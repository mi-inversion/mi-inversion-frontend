import React from 'react';
import { SidebarContainer, ItemsContainer } from './styled';
import ProfileImage from '../profile-image';
import { Typography } from '@material-ui/core';
import SidebarItem from './sidebar-item';

function Sidebar({ items, itemSelected, helloMessage, onItemSelect }) {
  return (
    <SidebarContainer>
      <ProfileImage />
      <Typography
        color="textPrimary"
        style={{ marginTop: '20px' }}
        variant="h6">
        {helloMessage}
      </Typography>
      <ItemsContainer>
        {items.map((item, index) => (
          <SidebarItem
            key={index}
            isSelected={index === itemSelected}
            name={item.name}
            icon={item.icon}
            onClick={() => {
              onItemSelect(index);
            }}
          />
        ))}
      </ItemsContainer>
    </SidebarContainer>
  );
}

export default Sidebar;
