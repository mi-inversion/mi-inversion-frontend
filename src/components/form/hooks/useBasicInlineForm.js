import { useState } from 'react';

function useBasicInlineForm(formButtonLabel, formControls, onSend) {
  const [data, setData] = useState({});

  function changeForm(name, value) {
    const newData = { ...data };
    newData[name] = value;
    setData(newData);
  }

  return {
    form: {
      formControls,
      onFormChange: changeForm,
      formData: data,
      onSend,
      sendButtonLabel: formButtonLabel,
    },
  };
}

export default useBasicInlineForm;
