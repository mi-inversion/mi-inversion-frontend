import React from 'react';
import { FormContainer } from './styled';
import { Typography } from '@material-ui/core';

function Form({
  style,
  className,
  formControls,
  onFormChange,
  formData,
  error,
}) {
  return (
    <FormContainer {...{ style, className }}>
      {formControls.map((formControl, key) => (
        <formControl.FormType
          key={key}
          label={formControl.label}
          value={formData[formControl.name]}
          variant="outlined"
          onInput={(ev) => {
            onFormChange(formControl.name, ev.target.value);
          }}
          fullWidth
          {...formControl}
        />
      ))}
      {error && (
        <Typography variant="caption" color="error">
          {error}
        </Typography>
      )}
    </FormContainer>
  );
}

export default Form;
