import React from 'react';
import './App.css';
import MainRouter from './router';
import { StringProvider } from './providers/string.provider';
import { AuthProvider } from './providers/auth.provider';

function App() {
  return (
    <StringProvider>
      <AuthProvider>
        <MainRouter />
      </AuthProvider>
    </StringProvider>
  );
}

export default App;
